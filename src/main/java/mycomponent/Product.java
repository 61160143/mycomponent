/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycomponent;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"espreso 1",40,"1.jpg"));
        list.add(new Product(2,"espreso 2",50,"1.jpg"));
        list.add(new Product(3,"espreso 3",60,"1.jpg"));
        list.add(new Product(4,"espreso 4",70,"1.jpg"));
        list.add(new Product(5,"espreso 5",80,"1.jpg"));
        list.add(new Product(1,"Americano 6",60,"2.jpg"));
        list.add(new Product(1,"milk 7",30,"3.jpg"));
        list.add(new Product(1,"milk 8",40,"3.jpg"));
        list.add(new Product(1,"milk 9",50,"3.jpg"));
        return list;
    }
}
